package coop.hackathon.ncr.com.context_aware_budgeting;

import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageButton;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        ImageButton ibAnal = (ImageButton) findViewById(R.id.imgbtn_Anal);
        ibAnal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadProfileAnalytics(v);
            }
        });

        ImageButton ibRec = (ImageButton) findViewById(R.id.imgbtn_Rec);
        ibRec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, 6);
                loadReceiptInfo(v);
            }
        });

        ImageButton ibPro = (ImageButton) findViewById(R.id.imgbtn_Plat);
        ibPro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadPromotionActivity(v);
            }
        });
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        //createNavigationDrawer(savedInstanceState, toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void loadProfileAnalytics(View view) {
        Intent intent = new Intent(this, ProfileAnalytics.class);
        startActivity(intent);
    }

    public void loadReceiptInfo(View view) {
        Intent intent = new Intent(this, ReceiptInfo.class);
        startActivity(intent);
    }

    public void loadPromotionActivity(View view) {
        Intent intent = new Intent(this, PromotionActivity.class);
        startActivity(intent);
    }
}

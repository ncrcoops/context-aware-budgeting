package coop.hackathon.ncr.com.context_aware_budgeting;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;
import java.util.Random;


public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private List<String> recieptList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView item, price;

        public MyViewHolder(View view) {
            super(view);
            item = (TextView) view.findViewById(R.id.item);
            price = (TextView) view.findViewById(R.id.price);
        }
    }


    public MyAdapter(List<String> recieptList) {
        this.recieptList = recieptList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_text_view, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Random rand = new Random();
        String reciept = recieptList.get(position);
        holder.item.setText(reciept);
        int quan = rand.nextInt(10)+1;
        holder.price.setText("Quantity: "+ quan);
    }

    @Override
    public int getItemCount() {
        return recieptList.size();
    }
}
# Context-Aware-Budgeting

## Description 

- Designed during NCR Waterloo Hackathon 2018. An Android application designed to scan in information using receipts, location, and financial transactions and use this information to create a personalized savings finder for the user.

- Finished as the runner-up contender to participate in the Global NCR hackathon in Atlanta

## Prerequisites & Installation

- Latest stable Android Studio in order to build and run project





